<div class="data view">
<h2><?php echo __('Data'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($data['Data']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($data['Data']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lastname'); ?></dt>
		<dd>
			<?php echo h($data['Data']['lastname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($data['Data']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($data['Data']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cel'); ?></dt>
		<dd>
			<?php echo h($data['Data']['cel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($data['Data']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($data['Data']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($data['Data']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Data'), array('action' => 'edit', $data['Data']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Data'), array('action' => 'delete', $data['Data']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $data['Data']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Data'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Data'), array('action' => 'add')); ?> </li>
	</ul>
</div>
