<style>
		table {
			border-collapse: collapse;
			margin-top: 30px;
		}

		table, th, td {
			border: 1px solid black;
			min-width: 170px;
			padding-left: 5px;
		}

		fieldset, .submit {
			display: inline-block;
		}
</style>

<div class="data form">
<?php echo $this->Form->create('Data'); ?>
	<fieldset>
	<?php
		echo $this->Form->input('qty', array('label' => 'Indique la cantidad de ganadores a elegir: ', 'type' => 'number', 'min' => 0));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Ver')); ?>
</div>
<?php if($data != false):?>
		<table>
			<tr>
				 <th><?= __('Nombre'); ?></th>
				 <th><?= __('Apellido'); ?></th>
				 <th><?= __('Correo'); ?></th>
				 <th><?= __('Teléfono'); ?></th>
				 <th><?= __('Celular'); ?></th>
				 <th><?= __('Código Promocional'); ?></th>
				 <th><?= __('Fecha de ingreso'); ?></th>
			</tr>
			<?php foreach($data as $el): ?>
					<tr>
							<td><?=$el['Data']['name']?></td>
							<td><?=$el['Data']['lastname']?></td>
							<td><?=$el['Data']['email']?></td>
							<td><?=$el['Data']['tel']?></td>
							<td><?=$el['Data']['cel']?></td>
							<td><?=$el['Data']['code']?></td>
							<?php $date = new DateTime($el['Data']['created']); ?>
							<td><?=$date->format('d-m-Y')?></td>
					</tr>
			<?php endforeach; ?>
		</table>
<?php endif; ?>

<script>
	$(document).ready(function(){
		$("#DataQty").keypress(function(event) {
		  // Backspace, tab, enter, end, home, left, right
		  // We don't support the del key in Opera because del == . == 46.
		  var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		  // IE doesn't support indexOf
		  var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		  // Some browsers just don't raise events for control keys. Easy.
		  // e.g. Safari backspace.
		  if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		      (49 <= event.which && event.which <= 57) || // Always 1 through 9
		      (48 == event.which && $(this).attr("value")) || // No 0 first digit
		      isControlKey) { // Opera assigns values for control keys.
		    return;
		  } else {
		    event.preventDefault();
		  }
		});
	});
</script>
