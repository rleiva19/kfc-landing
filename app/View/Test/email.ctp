<div class="main-container">
    <div class="header-container">
        <div class="wrapper">
            <nav class="nav-bar">
                <ul>
                    <li><a href="img/Reglamento-viaja-a-los-grande-con-mama-KFC.pdf" target="_blank">Reglamento</a></li>
                    <!--<li><a href="#">Ganadores</a></li>-->
                </ul>
            </nav>
        </div>
        <div class="slider">
            <img src="img/kfc/bg-sea.jpg">
            <div class="content-logos">
                <img src="img/kfc/sprite-header.png">
            </div>
        </div>
    </div>
    <div class="main wrapper clearfix"  id="form">
        <h1><img src="img/kfc/texts.svg"></h1>
        <div class="form-box">
            <div class="center-form">
                <img src="img/kfc/logo-kfc.png" class="logo-kfc-form">
                <?php echo $this->Session->flash(); ?>
                <?php echo $this->Form->create(false); ?>
                <?php echo $this->Form->input('name', array('label' => 'Nombre:', 'required' => true)); ?>
                <?php echo $this->Form->input('lastname', array('label' => 'Apellidos:', 'required' => true)); ?>
                <?php echo $this->Form->input('email', array('label' => 'Correo Electrónico:', 'required' => true)); ?>
                <?php echo $this->Form->input('tel', array('label' => 'Teléfono:', 'required' => true)); ?>
                <?php echo $this->Form->input('cel', array('label' => 'Cédula:', 'required' => true, 'type' => 'number')); ?>
                <div class="input text wrapper-code">
                    <?php echo $this->Form->input('code', array('label' => 'Código Promocional:', 'div' => false, 'required' => true)); ?>
                    <a href="#" class="tooltip">Ayuda</a>
                    <div class="wrapper-tooltip">
                        <img src="img/kfc/hand-help.png">
                    </div>
                </div>
                <!--                <div id="contentCaptcha">
                                    <div class="g-recaptcha" data-sitekey="6Le3egwTAAAAAFYGCkmEC-3ZQiCJNyFGOPdUKzlU" style="padding: 0;margin: 0;transform:scale(0.55);transform-origin:-19px -2px;-webkit-transform:scale(0.55);transform:scale(0.55);-webkit-transform-origin:-19px -2px;transform-origin:-19px -2px;"></div>
                                    <div class="alertCaptcha">Please verify Captcha</div>-->
                <div class="policies">
                    <div class="squaredFour">
                        <input type="checkbox" value="None" id="squaredFour" name="check" />
                        <span for="squaredFour"></span>
                    </div>
                    <label>He leído el <a href="img/Reglamento-viaja-a-los-grande-con-mama-KFC.pdf" target="_blank">reglamento</a> y acepto las condiciones</label>
                </div>
                <?php echo $this->Form->end('Enviar'); ?>
                <article class="text-help">
                    En caso de inconvenientes contáctenos:  <a href="tel:+40557000">4055-7000</a> /  <a href="mailto:reporte@kfccostarica.com">reporte@kfccostarica.com</a>
                </article>
            </div>
        </div>
    </div>
    <img src="img/kfc/logo-viajescolon.png" class="logo-viajescolon">
</div>
<footer class="footer">
    <nav class="nav-footer wrapper">
        <ul>
            <li><a href="img/Reglamento-viaja-a-los-grande-con-mama-KFC.pdf" target="_blank">Reglamento</a></li>
            <!--<li><a href="#">Ganadores</a></li>-->
            <li><a href="#">Seguínos en:</a></li>
            <li><a href="https://www.facebook.com/KFCCostaRica" target="_blank"><img src="img/kfc/facebook-logo.png" class="logo-facebook"></a></li>
        </ul>
    </nav>
</footer>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
    //Validación para que agarre solo números
    $("#code").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode == 67 && e.ctrlKey === true) ||
            (e.keyCode == 88 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    //Cuando el formulario se va a enviar, captura el submit, y antes valida si es un código válido o no
    $("#emailForm").submit(function(){
        //checkear políticas
        if(!$("#squaredFour").is(":checked")){
            alert('Debe leer el reglamento y aceptar las condiciones.');
            return false;
        }

        var number = $("#code").val();
        var array = number.split("");

        if(array.length == 10){
            var store = array[0] + array[1];
            var storeName = getStoreName(store);

            if((storeName != "no") && getFirstMinute(array[3]) && getFirstDate(array[6])){
                console.log("El código " + number + " es válido y fue generado en: " + storeName);
            } else {
                alert("El código ingresado no es válido");
                return false;
            }
        } else {
            alert("El código ingresado no es válido");
            return false;
        }
    });
});

/**** funciones de validación ****/
function getFirstMinute(firstMinute){
    if(parseInt(firstMinute) >= 0 && parseInt(firstMinute) <= 5){
        return true;
    } else {
        return false;
    }
}
function getFirstDate(firstDate){
    if(parseInt(firstDate) >= 0 && parseInt(firstDate) <= 3){
        return true;
    } else {
        return false;
    }
}
function getStoreName(store){
    switch(store){
      case "01":
        return "Paseo Colon";
      case "02":
        return "La California";
      case "03":
        return "Embajada";
      case "04":
        return "Mall San Pedro";
      case "05":
        return "Real Cariari";
      case "06":
        return "Plaza del Sol";
      case "07":
        return "Heredia";
      case "08":
        return "Escazú";
      case "09":
        return "Multi. Escazu";
      case "10":
        return "Mall Alajuela";
      case "11":
        return "Palace";
      case "12":
        return "Presidente";
      case "13":
        return "Colegios";
      case "14":
        return "San Carlos";
      case "15":
        return "Multi. Este";
      case "16":
        return "Terramall";
      case "17":
        return "Guadalupe";
      case "18":
        return "Paseo las Flores";
      case "24":
        return "Lindora";
      case "21":
        return "Jaco";
      case "19":
        return "Alajuela";
      case "22":
        return "Desamparados";
      case "23":
        return "Grecia";
      case "25":
        return "Cartago";
      case "26":
        return "Plaza Mayor";
      case "27":
        return "Plaza San Gabriel";
      case "29":
        return "Cartago Ruinas";
      case "30":
        return "Plaza Lincoln";
      case "31":
        return "Paseo Metrópoli";
      case "32":
        return "Tibas";
      case "33":
        return "San Rafael Abajo";
      case "35":
        return "Zona Centro";
      case "34":
        return "City Mall";
      case "36":
        return "Nicoya";
      case "37":
        return "Terrazas Lindora";
      default:
        return "no";
    }
}
/**** funciones de validación ****/
</script>
