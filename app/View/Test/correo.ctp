<?php if ($page != 'gracias') { ?>
    <div id="wrap-landing">
        <header>
            <?php echo $this->Html->image('wam/logo-wamDigital.png', array("class" => "wamdigital-logo", "title" => "Wam Digital", "alt" => "Wam Digital")); ?>
        </header>
        <main>
            <div class="col-left">
                <h1>
                    <?php echo $this->Html->image('wam/copy-left_es.png', array('title' => "Estamos listos para producir sus banners en HTML5", 'alt' => 'Estamos listos para producir sus banners en HTML5')); ?>
                    <article>
                        <p>
                            <strong>En Costa Rica: <a href="tel:+5062524-1692">(506) 2524-1692</a></strong><br><strong>In the U.S.A.: <a href="tel:+5062524-1692">(773) 382-8683</a></strong>
                        </p>
                        <p>
                            <a href="mailto:marketing@wamdigital.com">marketing@wamdigital.com</a>
                        </p>
                    </article>
                </h1>
            </div>
            <div class="col-right">
                <div class="content-text">
                    <p>
                        ¿Sabías que tus banners de publicidad en línea hechos en Flash ya no corren en los 3 navegadores más populares: Firefox, Chrome y Safari?
                        <br>Ahora toda la publicidad en línea se debe hacer en HTML5, y para que su publicidad venda como debe ser, estamos listos para producir todos sus banners para que se ejecutan sin problemas en cualquier navegador y cualquier dispositivo móvil.
                    <p/>
                    <p>
                        En WAM Digital, somos expertos en publicidad en línea en HTML5, si usted tiene alguna pregunta o desea más información, por favor escríbanos o llámenos y con gusto lo dirigimos en la dirección correcta.
                    </p>
                </div>
                <div class="form-box">
                    <label>Contáctenos.</label>
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Form->create(false); ?>
                    <?php echo $this->Form->input('name', array('label' => false, 'placeholder' => 'Nombre', 'required' => true)); ?>
                    <?php echo $this->Form->input('email', array('label' => false, 'placeholder' => 'Email', 'required' => true)); ?>
                    <?php echo $this->Form->input('about', array('type' => 'textarea', 'label' => false, 'class' => 'es', 'placeholder' => 'Sobre su proyecto', 'required' => true)); ?>
                    <div id="contentCaptcha">
                        <div class="g-recaptcha" data-sitekey="6Le3egwTAAAAAFYGCkmEC-3ZQiCJNyFGOPdUKzlU" style="padding: 0;margin: 0;transform:scale(0.55);transform-origin:-19px -2px;-webkit-transform:scale(0.55);transform:scale(0.55);-webkit-transform-origin:-19px -2px;transform-origin:-19px -2px;"></div>
                        <div class="alertCaptcha">Verificar el captcha</div>
                    </div>
                    <?php echo $this->Form->end('Submit'); ?>
                </div>
                <article>
                    <p>
                        <strong>En Costa Rica: <a href="tel:+5062524-1692">(506) 2524-1692</a></strong><br><strong>In the U.S.A.: <a href="tel:+5062524-1692">(773) 382-8683</a></strong>
                    </p>
                </article>
            </div>
        </main>
        <footer>
            <div class="content-logos">
                <?php echo $this->Html->image('wam/icon-iab.png', array("class" => "logo-iab", "title" => "iab logo", "alt" => "iab logo")); ?>
                <?php echo $this->Html->image('wam/icon-celtra.png', array("class" => "logo-celtra", "title" => "celtra", "alt" => "celtra logo")); ?>
                <?php echo $this->Html->image('wam/icon-mediamind.png', array("class" => "logo-mediamind", "title" => "Media Mind logo", "alt" => "Media Mind logo")); ?>
                <?php echo $this->Html->image('wam/icon-pointroll.png', array("class" => "logo-PointRoll", "title" => "PointRoll logo", "alt" => "PointRoll logo")); ?>
                <?php echo $this->Html->image('wam/icon-doubleClick.png', array("class" => "logo-doubleClick", "title" => "DoubleClick logo", "alt" => "DoubleClick logo")); ?>
                <?php echo $this->Html->image('wam/icon-adwards.png', array("class" => "logo-googleadwards", "title" => "Google Adwards logo", "alt" => "Google Adwards logo")); ?>
            </div>
        </footer>
    </div>
<?php } else { ?>
    <div id="wrap-landing">
        <header>
            <a href="<?php echo $this->Html->url('/es'); ?>">
                <?php echo $this->Html->image('wam/logo-wamDigital.png', array("class" => "wamdigital-logo", "title" => "Wam Digital", "alt" => "Wam Digital")); ?>
            </a>
        </header>
        <main>
            <p class="copy_thank es">
                Gracias
            </p>
            <p class="copy_sent es">
                su correo fue enviado
            </p>
        </main>
        <footer>
            <div class="content-logos">
                <?php echo $this->Html->image('wam/icon-iab.png', array("class" => "logo-iab", "title" => "iab logo", "alt" => "iab logo")); ?>
                <?php echo $this->Html->image('wam/icon-celtra.png', array("class" => "logo-celtra", "title" => "celtra", "alt" => "celtra logo")); ?>
                <?php echo $this->Html->image('wam/icon-mediamind.png', array("class" => "logo-mediamind", "title" => "Media Mind logo", "alt" => "Media Mind logo")); ?>
                <?php echo $this->Html->image('wam/icon-pointroll.png', array("class" => "logo-PointRoll", "title" => "PointRoll logo", "alt" => "PointRoll logo")); ?>
                <?php echo $this->Html->image('wam/icon-doubleClick.png', array("class" => "logo-doubleClick", "title" => "DoubleClick logo", "alt" => "DoubleClick logo")); ?>
                <?php echo $this->Html->image('wam/icon-adwards.png', array("class" => "logo-googleadwards", "title" => "Google Adwards logo", "alt" => "Google Adwards logo")); ?>
            </div>
        </footer>
    </div>
<?php
}
