<div class="main-container voffset-4">
    <div class="main wrapper clearfix">
        <div class="form-box">
            <div class="center-form">
                <img src="img/kfc/logo-kfc.png" class="logo-kfc-form">
                <h2 class="text-thaks"><img src="img/kfc/thanks-text.svg"></h2>
                <p class="" class="text-body"><img src="img/kfc/text-body.svg"></p>
                <a href="http://promokfc.com/" class="btn-back">Registrar otro código</a>
                <a href="https://www.facebook.com/KFCCostaRica" class="btn-go" target="_blank">Ir a Facebook</a>
            </div>
        </div>
    </div>
    <img src="img/kfc/logo-viajescolon.png" class="logo-viajescolon">
</div>
<footer class="footer">
    <nav class="nav-footer wrapper">
        <ul>
            <li><a href="img/Reglamento-viaja-a-los-grande-con-mama-KFC.pdf" target="_blank">Reglamento</a></li>
            <!--<li><a href="#">Ganadores</a></li>-->
            <li><a href="#">Seguínos en:</a></li>
            <li><a href="https://www.facebook.com/KFCCostaRica" target="_blank"><img src="img/kfc/facebook-logo.png" class="logo-facebook"></a></li>
        </ul>
    </nav>
</footer>
</div>
