<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TestController extends AppController {

    public $components = array('Paginator', 'Flash', 'Session');
    public $uses = array('Data');

    public function email() {
        if ($this->request->is('post')) {
            $data = $this->request->data;

            $exists = $this->Data->find('first', array('conditions' => array('code' => $data['code'])));

            if(isset($exists['Data']['id'])){
                $this->Flash->error(__('¡Su código promocional había sido activado anteriormente!'));
                return $this->redirect(array('action' => 'email'));
            } else {
                $this->Data->create();
                $this->Data->save($data);
                //$this->Flash->success(__('¡Tus datos fueron guardados con éxito!'));
                return $this->redirect(array('action' => 'thanks'));
            }
        }
    }
    public function thanks(){

    }

    public function correo($page = null){
        if ($this->request->is('post')) {
            $data = $this->request->data;


        }
        $this->set('page', $page);
    }

    public function gracias(){}

}
